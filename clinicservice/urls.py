from django.urls import path
from .views import ClinicServiceView

urlpatterns = [
    path('clinicport/', ClinicServiceView.as_view(), name='url_clinic')
]
