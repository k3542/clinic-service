from urllib import response
from django.shortcuts import render
from django.http import FileResponse, HttpResponse
from rest_framework.response import Response
from rest_framework.views import APIView
import asyncio
import json
from django.conf import settings
from hachiclinic.tasks import diagnose_disease
from .serializers import *

# Create your views here.
class ClinicServiceView(APIView):    
    def post(self, request, format=None):
        serializer = ClinicSendSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data

        diagnose_disease.delay(data)
        response = Response("OK", headers = {"Access-Control-Allow-Origin":"*"},status=200)    
        return response
        