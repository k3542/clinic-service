from rest_framework import serializers


class ClinicSendSerializer(serializers.Serializer):
    clinic_visit_id = serializers.IntegerField()
    symptoms = serializers.ListField(child=serializers.CharField(), default=[])

class ClinicResultSerializer(serializers.Serializer):
    clinic_visit_id = serializers.IntegerField()
    doctor_recipes = serializers.ListField(child=serializers.CharField())
    diagnosis = serializers.CharField(max_length=255)