import string
import time
import requests

from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from celery import shared_task

from clinicservice.serializers import *

@shared_task
def diagnose_disease(info):
    time.sleep(10)

    symptoms_ctr = 0

    for symptom in info["symptoms"]:
        if symptom == "rash":
            symptoms_ctr+=1
        elif symptom == "pox":
            symptoms_ctr+=2
        elif symptom == "coughing":
            symptoms_ctr+=3
        elif symptom == "sneezing":
            symptoms_ctr+=4
        elif symptom == "fever":
            symptoms_ctr+=5
        elif symptom == "stomachache":
            symptoms_ctr+=6
        else:
            symptoms_ctr+=0
    

    diagnosis = ""
    doctor_recipes = []

    if symptoms_ctr <= 7:
        diagnosis = "Mad Snail Disease"
        doctor_recipes = ["ibuprofen"]
    elif symptoms_ctr <=14:
        diagnosis = "Monkeypox"
        doctor_recipes = ["methyl", "antacid", "diclofenac", "amoxapine"]
    else:
        diagnosis = "Sawhii Flu"
        doctor_recipes = ["valsartan", "paracetamol"]

    dic = {"clinic_visit_id":info["clinic_visit_id"],"diagnosis":diagnosis, "doctor_recipes":doctor_recipes}

    serializer = ClinicResultSerializer(data=dic)
    serializer.is_valid(raise_exception=True)
    data = serializer.data

    url = "http://0.0.0.0:8000/clinicport/"
    headers = {'Content-Type': "application/json", 'Accept': "application/json"}
    res = requests.post(url, json=data, headers=headers)